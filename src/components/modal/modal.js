import React from 'react';
import './modal.css';
import FontAwesome from 'react-fontawesome';

const Modal = (props) => {
  const { closeModal } = props;

  const closeicon = () => (
    <FontAwesome
      name="times"
      id="times-id"
      onClick={closeModal}
      style={{
        color: '#000000',
        padding: '10px',
        cursor: 'pointer',
        backgroundColor: 'transparent',
        border: 0,
        position: 'absolute',
        top: '0.3rem',
        right: '0.5rem',
      }}
    />
  );

  return (
    <div className="modal fade" id="exampleModal" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
      <div className="modal-dialog modal-dialog-centered modal-lg" role="document" >
        <div className="modal-content">
          <div className="modal-body">
            <button type="button" className="close close1" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true"><i className="fas fa-times"></i></span>
            </button>
            {props.children}
          </div>
        </div>
      </div>
    </div>
  );
};


export default Modal;
