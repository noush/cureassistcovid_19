import React from 'react';
import { Link, Redirect } from 'react-router-dom';
import '../../css/index.css'
import Modal from "react-responsive-modal";
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

export default class Navbar extends React.Component {
    google={};
    constructor() {
        super()
        this.state = {
            posts: [],
            isLoading: true,
            login: false,
            fields: {},
            errors: {},
            data: {}
        }
    }

    saveProfile = () => {
        let name = this.state.fields["name"]
        let email = this.state.fields["email"]
        let phone = this.state.fields["phone"]
        let user = {
            "mobile": this.state.fields["phone"],
            "user": {
                "name": this.state.fields["name"],
                "email": this.state.fields["email"]
            }
        }
        fetch('http://139.59.17.158:8000/tracker/saveProfile', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-type': 'application/json'
            },
            body: JSON.stringify({
                "mobile": phone,
                "user": {
                    "name": name,
                    "email": email
                }
            }),
        })
            .then((response) => response.json())
            .then((responseJson) => {

                localStorage.setItem("user", JSON.stringify(user));
                this.setState({ login: false });
                toast("User Profile Saved");

            })
            .catch((error) => {
            });
    }


    onOpenModal = () => {

        this.setState({ login: true });
    };

    onCloseModal = () => {
        this.setState({ login: false });
    };

    // onSubmit = () => {
    //     this.saveProfile();
    // }


    handleValidation() {
        let fields = this.state.fields;
        let errors = {};
        let formIsValid = true;

        // //Name
        // if (!fields["name"]) {
        //     formIsValid = false;
        //     errors["name"] = "Cannot be empty";
        // }

        if (typeof fields["phone"] !== "undefined") {
            if (!fields["phone"].match(/^\d{10}$/)) {
                formIsValid = false;
                errors["phone"] = "Please enter a 10-digit Phone Number";
            }
        }
        //phone
        if (!fields["phone"]) {
            formIsValid = false;
            errors["phone"] = "Cannot be empty";
        }

        //Email
        // if (!fields["email"]) {
        //     formIsValid = false;
        //     errors["email"] = "Cannot be empty";
        // }

        // if (typeof fields["email"] !== "undefined") {
        //     let lastAtPos = fields["email"].lastIndexOf('@');
        //     let lastDotPos = fields["email"].lastIndexOf('.');

        //     if (!(lastAtPos < lastDotPos && lastAtPos > 0 && fields["email"].indexOf('@@') == -1 && lastDotPos > 2 && (fields["email"].length - lastDotPos) > 2)) {
        //         formIsValid = false;
        //         errors["email"] = "Email is not valid";
        //     }
        // }



        this.setState({ errors: errors });
        return formIsValid;
    }

    contactSubmit(e) {
        e.preventDefault();
        if (this.handleValidation()) {
            this.saveProfile();

        } else {
            toast("Form has errors");
        }

    }

    handleChange(field, e) {
        let fields = this.state.fields;
        fields[field] = e.target.value;
        this.setState({ fields });
        
    }


    componentDidMount() {
        if (!window.google) {
            this.loadMap().then((loaded) => {
                if (loaded) {
                    this.google = window.google;
                    
                }
            });
        }
         else {
           
            this.google = window.google;
           
         }

        //fetch('http://139.59.17.158:8000/tracker/readStats', {
            fetch('http://139.59.17.158:8000/tracker/stats', {
            method: 'GET'
        })
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({
                    data: responseJson.data
                })
            })
            .catch((error) => {
            });


    }

    loadMap() {
        return new Promise((resolve) => {
          window.initMap = (ev) => {
            resolve(true);
          };
          var script = document.createElement('script');
          script.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyB7s8nphRNHn90429k_pVywvt-QOhyWSJk&libraries=places,visualization&callback=initMap';
          script.type = 'text/javascript';
          script.async = true;
          script.defer = true;
          document.getElementsByTagName('head')[0].appendChild(script);
    
        });
      }


    NavigateToDashboard = (route) => {
        // console.log(this.props.history)
        this.props.history.pop(route);
    }

    render() {
        toast.configure({
            autoClose: 8000,
            draggable: false,
            //etc you get the idea
        });

        return (
            <div>
                <header>
                    <div className="container">
                        <div className="row " style={{marginBottom:20}}>
                            <div className="col-sm-6 col-md-6 col-8">
                                <ul className="back-links">
                                    <li><Link to="/" style={{ textDecoration: 'none' }} className="header" id="back"><i className="fas fa-arrow-left" ></i></Link></li>
                                    <li> <Link to="/" style={{ textDecoration: 'none' }} className="header">
                                        <h4 style={{ color: '#000' }}>Corona Tracker</h4>
                                    </Link></li>

                                </ul>
                            </div>
                            <div className="col-sm-6 col-md-6 col-4">
                                <ul className="list-items">
                                    <li>
                                        <div className="login">
                                            <button className="btn btn-primary-outline" id="signup"
                                                onClick={this.onOpenModal}
                                                style={{ alignSelf: 'center', alignContent: 'center', justifyContent: 'center' }}>
                                                <i className="fas fa-user" style={{ alignSelf: 'center' }}></i></button>
                                        </div>
                                    </li>
                                </ul>
                            </div>

                        </div>
                    </div>
                </header>
                <section className="live-counter">

                    <div className="container" >

                        <div className="live-row">
                            <div className="row ">
                                <div className="col-sm-4 col-4 col-md-4">
                                    <div className="live-cnt">
                                        <h3>{this.state.data.infected}</h3>
                                        <label>Infected</label>
                                        <div className="circle"></div>
                                    </div>
                                </div>
                                <div className="col-sm-4 col-4 col-md-4">
                                    <div className="live-cnt">
                                        <h3>{this.state.data.cured}</h3>
                                        <label>Recovered</label>
                                        <div className="circle" style={{ backgroundColor: "green" }}></div>
                                    </div>
                                </div>
                                <div className="col-sm-4 col-4 col-md-4">
                                    <div className="live-cnt">
                                        <h3>{this.state.data.deaths}</h3>
                                        <label>Fatalities</label>
                                        <div className="circle" style={{ backgroundColor: 'red' }}></div>
                                    </div>            </div>
                            </div>
                        </div>
                    </div>
                </section>

                <Modal  open={this.state.login} onClose={this.onCloseModal}>
                    <div className="modal-body" id="userDetails"  style={{  }}>
                        <form name="contactform" className="contactform" onSubmit={this.contactSubmit.bind(this)}>
                            <div className="col-sm-12 col-12 col-md-12">
                                <fieldset >
                                    <label style={{ textAlign: "center", flexDirection: 'row', flexWrap: 'wrap' }}>User Details</label>
                                    <label className="required">fields with <span style={{color:'red'}}>*</span> are mandatory</label>
                                    <input className="form-control" refs="phone" type="text" size="30" placeholder="Phone *" onChange={this.handleChange.bind(this, "phone")} value={this.state.fields["phone"]} required />
                                    <span className="error">{this.state.errors["phone"]}</span>
                                    <div style={{ marginBottom: 10 }}></div>

                                    <input className="form-control" ref="name" type="text" size="30" placeholder="Name" onChange={this.handleChange.bind(this, "name")} value={this.state.fields["name"]} />
                                    <span className="error">{this.state.errors["name"]}</span>
                                    <div style={{ marginBottom: 10 }}></div>
                                    <input className="form-control" refs="email" type="email" size="30" placeholder="Email" onChange={this.handleChange.bind(this, "email")} value={this.state.fields["email"]} />
                                    <span className="error">{this.state.errors["email"]}</span>
                                    <div style={{ marginBottom: 10 }}></div>


                                </fieldset>
                            </div>
                            
                            <div className="col-md-12">
                            
                                <fieldset style={{ textAlign: "center" }}>
                                    <button className="btn btn-lg pro btn-block" id="submit" value="Submit" onSubmit={() => this.contactSubmit()}>Submit</button>
                                </fieldset>

                            </div>
                        </form>
                    </div>
                </Modal>
            </div>

        )
    }
}

