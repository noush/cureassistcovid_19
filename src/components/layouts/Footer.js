import React from 'react';
import img from '../../images/cureAssit_logo.png'
function Footer() {
  return (
    <footer>
      <div className="container">
        <p>powered by <img src={img} />ureAssist </p>
      </div>
    </footer>
  );
}
export default Footer;

