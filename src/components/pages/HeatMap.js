import React from 'react';
import { Link } from 'react-router-dom';
import $ from 'jquery'
import { BallBeat } from 'react-pure-loaders';
import '../../css/index.css'

export default class Map extends React.Component {
  google = {};
  map = {};
  markers = [];
  markerslist = [];
  localUser = {};
  loc = [];
  statename = [];
  heatmap = '';
  constructor() {
    super()
    this.state = {
      loading: true,
     // data :[],
      data: [
       {name: "Andhra Pradesh", latitude: 15.9128998, longitude: 79.7399875, count: 88},
 {name: "Andaman and Nicobar Islands", latitude: 11.7400867, longitude: 92.6586401, count: 10},
 {name: "Assam", latitude: 26.2006043, longitude: 92.9375739, count: 1},
 {name: "Bihar", latitude: 25.0960742, longitude: 85.31311939999999, count: 24},
 {name: "Chandigarh", latitude: 30.7333148, longitude: 76.7794179, count: 16},
 {name: "Chhattisgarh", latitude: 21.2786567, longitude: 81.8661442, count: 11},
 {name: "Delhi", latitude: 28.7040592, longitude: 77.10249019999999, count: 159},
 {name: "Goa", latitude: 15.2993265, longitude: 74.12399599999999, count: 4},
 {name: "Gujarat", latitude: 22.258652, longitude: 71.1923805, count: 92},
 {name: "Haryana", latitude: 29.0587757, longitude: 76.085601, count: 50},
 {name: "Himachal Pradesh", latitude: 31.1048294, longitude: 77.17339009999999, count: 5},
 {name: "Jammu and Kashmir", latitude: 33.778175, longitude: 76.57617139999999, count: 66},
 {name: "Jharkhand", latitude: 23.6101808, longitude: 85.2799354, count: 1},
 {name: "Karnataka", latitude: 15.3172775, longitude: 75.7138884, count: 122},
 {name: "Kerala", latitude: 10.8505159, longitude: 76.2710833, count: 284},
 {name: "Ladakh", latitude: 35.0410896, longitude: 77.9370979, count: 16},
 {name: "Madhya Pradesh", latitude: 22.9734229, longitude: 78.6568942, count: 105},
 {name: "Maharashtra", latitude: 19.7514798, longitude: 75.7138884, count: 387},
 {name: "Manipur", latitude: 24.6637173, longitude: 93.90626879999999, count: 1},
 {name: "Mizoram", latitude: 23.164543, longitude: 92.9375739, count: 1},
 {name: "Odisha", latitude: 20.9516658, longitude: 85.0985236, count: 4},
 {name: "Puducherry", latitude: 11.9415524, longitude: 79.8082865, count: 4},
 {name: "Punjab", latitude: 31.1471305, longitude: 75.34121789999999, count: 51},
 {name: "Rajasthan", latitude: 27.0238036, longitude: 74.21793260000001, count: 109},
 {name: "Tamil Nadu", latitude: 11.1271225, longitude: 78.6568942, count: 235},
 {name: "Telengana", latitude: 18.1124372, longitude: 79.01929969999999, count: 90},
 {name: "Uttarakhand", latitude: 30.066753, longitude: 79.01929969999999, count: 8},
 {name: "Uttar Pradesh", latitude: 26.8467088, longitude: 80.9461592, count: 128},
 {name: "West Bengal", latitude: 22.9867569, longitude: 87.8549755, count: 46}
      ]

    }
  }
  componentDidMount() {
    $("#back").show();
    //getting local user details
    let user = JSON.parse(localStorage.getItem("user"));
    this.localUser = user;
    this.fetchData();
  }
  // componentWillUnmount = () => {
  //   this.heatmap.setMap(null);
  // }

  geocode = (placesResponse) => {
    this.setState({
      loading: false
    })
     //   var geocoder = new this.google.maps.Geocoder();

let cordnates = Object.values(this.state.data);
 cordnates.map((place,index)=>{
  // console.log(place.latitude);
 let lat = place.latitude;
 let lng = place.longitude;
 var random = [{ location: new this.google.maps.LatLng(lat, lng),weight :place.count/10}];
 this.heatmap = new this.google.maps.visualization.HeatmapLayer({
           data: random,
          });
       this.heatmap.setMap(this.map);
  })


     //for world heat map
// let cordnates = Object.values(placesResponse.data.locations);
//  cordnates.map((place,index)=>{
// let lat = place.coordinates.lat;
// let lng = place.coordinates.long;
//  var random = [{ location: new this.google.maps.LatLng(lat, lng),weight :place.count/50000}];
// this.heatmap = new this.google.maps.visualization.HeatmapLayer({
//           data: random,
//           radius : 10
//          });
//        this.heatmap.setMap(this.map);
//   })
// world heat map ends


    // this.statename.map((place, index) => {
    //   setTimeout(() => {
    //   geocoder.geocode({ 'address': place['state'] }, (results, status) => {
    //     if (status == 'OK') {
        
    //       let lat = results[0].geometry.location.lat();
    //       let lng = results[0].geometry.location.lng();
    //       var random = [{ location: new this.google.maps.LatLng(lat, lng), weight: place['count'] }];
    //        this.heatmap = new this.google.maps.visualization.HeatmapLayer({
    //         data: random
    //        });
    //       this.heatmap.setMap(this.map);
    //       //this.heatmap.set('radius',place['count']);
    //      }
    //   })
    // },index*1000);
    // })

    //  for(var i = 0; i < this.statename.length; i++) {


    //     setTimeout(() => {
    //       geocoder.geocode({ 'address': x['state'] }, (results, status) => {
    //         if(status == 'OK'){
    //           let lat= results[0].geometry.location.lat();
    //           let lng= results[0].geometry.location.lng();
    //          // console.log(x['state'])
    //          // console.log(lat+" "+lng);
    //           var random = [{location:new this.google.maps.LatLng(lat,lng),weight:x['count']}];
    //            this.heatmap = new this.google.maps.visualization.HeatmapLayer({
    //             data: random
    //           });
    //           this.heatmap.setMap(this.map);
    //           this.loc.push({
    //             name: x['state'],
    //             latitude: lat,
    //             longitude: lng,
    //             count: x['count']
    //           });
    //         }

    //     });

    //   },i*1500);

    // }



  }
  fetchData = () => {
    fetch('http://139.59.17.158:8000/tracker/readStats', {
      method: 'GET'
    })
      .then((response) => response.json())
      .then((responseJson) => {
     //   console.log(responseJson)
//    responseJson.map((place,index)=>{
// console.log(place)
//    })
if (window.google) {
              this.google = window.google;
              this.initMap(responseJson);
        }

      })
      .catch((error) => {
      });


  }


  // fetchData = () => {
  //   fetch('http://139.59.17.158:8000/tracker/readStats', {
  //     method: 'GET'
  //   })
  //     .then((response) => response.json())
  //     .then((responseJson) => {
  //       this.setState({
  //         data: responseJson.data
  //       },
  //         () => {
  //           this.testmapping(responseJson.data.locations);
  //         });

  //     }).then(() => {
  //       if (window.google) {
  //             this.google = window.google;
  //             this.initMap();
  //       }
       
  //     })
  //     .catch((error) => {
  //     });


  // }
  testmapping = (statesWise) => {
    Object.keys(statesWise).forEach((key) => {
      this.statename.push({ state: key, count: statesWise[key] });
    }
    );
  }
  setStorage = () => {
    localStorage.setItem("location", JSON.stringify(this.loc));
  }


  getPlaces = () => {
    fetch('http://139.59.17.158:8000/tracker/heatMap', { method: 'GET' })
      .then((response) => response.json())
      .then((responseJson) => {

        // this.setState({
        //   userInfo: responseJson
        // })

        responseJson.data.map((pos) => {

          if (pos.hasOwnProperty('place')) {

            this.addLocation(pos.place.geometry.location)
          }

        })


      })
      .catch((error) => {
      });
  }

  initMap(placesResponse) {
    // this.getPlaces();
    let that = this;
    this.map = new this.google.maps.Map(document.getElementById('map'), {
      zoom: 4,
     // center: new this.google.maps.LatLng(56, 10),//denmark
      center: new this.google.maps.LatLng(20.5937, 78.9629),//India
      mapTypeId: 'roadmap'
    });
    this.geocode(placesResponse);
    //this.testmapping(this.state.data.locations);
    // that.markerslist = [new this.google.maps.LatLng(12.963588, 77.712155),
    // new this.google.maps.LatLng(12.9063, 77.5857),
    // new this.google.maps.LatLng(12.9352, 77.6245)];
  }


  addLocation(loc) {

    new this.google.maps.Marker({ position: loc, map: this.map });
  }

  loadMap() {
    return new Promise((resolve) => {
      window.initMap = (ev) => {
        resolve(true);
      };
      var script = document.createElement('script');
      script.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyB7s8nphRNHn90429k_pVywvt-QOhyWSJk&libraries=places&libraries=visualization&callback=initMap';
      script.type = 'text/javascript';
      script.async = true;
      script.defer = true;
      document.getElementsByTagName('head')[0].appendChild(script);

    });
  }


  render() {
    return (
      <div>
        <div className="header hdr-h1" style={{}}>
          <div className="container">
            <h5>COVID-19 Heatmap  </h5>
          </div>
        </div>
        <div className="container">
       
          <div className="map-block">

          <div className='plus'>
        <BallBeat
          color={'#123abc'}
          loading={this.state.loading}
        />
      </div>
            <div className="map1" id="map"></div>
          </div>


        </div>

      </div>

    )
  }


}
