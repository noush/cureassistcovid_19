import React from 'react';
import ReactDOM from 'react-dom';
import Modal from 'react-responsive-modal';
 
export default class Suggestion extends React.Component {
  state = {
    open: true,
  };
 
  onOpenModal = () => {
    this.setState({ open: true });
  };
 
  onCloseModal = () => {
    this.setState({ open: false });
  };
 
  render() {
    const { open } = this.state;
    return(
      <div>
        <Modal open={open} onClose={this.onCloseModal} center>
        {/* <Modal open={this.props.value} onClose={this.onCloseModal} center> */}
          <h2>Your answers suggest that you are unlikely to be infected with COVID-19, but based on your symptoms you may have an upper respiratory tract virus.</h2>

          <h3>According to official instructions, you should act as follows:</h3>
          <div>
              <ol>
                  <li>
                      <h4>Stay at home for two weeks after symptoms emerge.</h4>
                  </li>
                  <li>
                      <h4>Monitor your health! If the condition worsens (breathing difficulties; fever > 100 degrees with dry cough) visit your doctor</h4>
                  </li>
                  <li>
                  If you are over 60 years old or you suffer from a chronic illness such as diabetes, lung disease or heart disease, monitor your health closely. If your symptoms persist beyond three days or worsen (fever >100degrees, breathing difficulties or general deterioration), contact a general practitioner immediately.
                  </li>
              </ol>
          </div>
          
        </Modal>
      </div>
    );
  }
}