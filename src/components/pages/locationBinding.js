import React from 'react';


export default class List extends React.Component{

    render(){
        return(<div className="card stack-card">
             <ul>
                {this.props.places.map((data, index) => (
                
                     <li  key={index} className="card crd-no">

                         <p>{data.duration} - </p>
                         
                         <span className="icon"><i className="fas fa-times"></i></span>
                        </li> 
             
                ))}
                        </ul>
                </div>)
        
    }
}