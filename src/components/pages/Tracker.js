import React from 'react';
import '../../css/index.css';
import styled from 'styled-components';

export default class Tracker extends React.Component {

	render() {
		return (

			<div>

				<div className="container">
					<section className="details">
						<div className="people met">
							<label>Place Visited</label>
							<div class="row">
								<div class="col-sm-10 col-10 col-md-10">
									<input type="number" class="form-control" placeholder="Mobile No." />
								</div>
								<div class="col-sm-2 col-2 col-md-2">
									<button type="button" class="btn btn-plus">
										<i class="fas fa-plus"></i>
									</button>
								</div>
							</div>
							<div class="owl-carousel owl-theme" id="owl-carousel2">
								<div class="item">
									<div class="card crd-no">

										<p>123455676</p>
										<span class="icon"><i class="fas fa-times"></i></span>
									</div>


								</div>
								<div class="item">
									<div class="card crd-no">

										<p>1234444</p>

										<span class="icon"><i class="fas fa-times"></i></span>
									</div>
								</div>
								<div class="item">
									<div class="card crd-no" >

										<p>12346788</p>

										<span class="icon"><i class="fas fa-times"></i></span>
									</div>
								</div>
								<div class="item">
									<h4>4</h4>
								</div>
								<div class="item">
									<h4>5</h4>
								</div>

							</div>
						</div>
						<label>Place Visited</label>

						<div class="row">
							<div class="col-sm-5 col-5 col-md-5">
								<div class="location">
									<input type="text" class="live-search-box form-control" placeholder="Location" />
									<ul class="live-search-list">
										<li>Lorem</li>
										<li>ipsum</li>
										<li>dolor</li>
										<li>sit</li>
										<li>amet</li>
									</ul>
								</div>
							</div>
							<div class="col-sm-5 col-5 col-md-5 no-padding" >
								<input type='text' class="form-control" id='datetimepicker1' placeholder="Duration" />
							</div>
							<div class="col-sm-2 col-2 col-md-2">
								<button type="button" class="btn btn-plus">
									<i class="fas fa-plus"></i>
								</button>
							</div>
						</div>
						<div class="map">
							<div id="map_container"></div>
							<div id="map"></div>
						</div>

					</section>
				</div>
				<section class="sub-details">
					<div class="owl-carousel owl-theme" id="owl-carousel1">
						<div class="item">
							<div class="card">

								<p style={{ marginBottom: 0, fontSize: 12, color: '#000', fontWeight: 400 }}>Vega City</p>
								<span style={{ fontSize: 10, color: '#888484' }}>13 Mar 2020-11.5PM</span>
								<span class="icon"><i class="fas fa-times"></i></span>
							</div>


						</div>
						<div class="item">
							<div class="card">

								<p>Vega City</p>
								<span>13 Mar 2020-11.5PM</span>
								<span class="icon"><i class="fas fa-times"></i></span>
							</div>
						</div>
						<div class="item">
							<div class="card">

								<p>Vega City</p>
								<span>13 Mar 2020-11.5PM</span>
								<span class="icon"><i class="fas fa-times"></i></span>
							</div>
						</div>
						<div class="item">
							<h4>4</h4>
						</div>
						<div class="item">
							<h4>5</h4>
						</div>

					</div>
				</section>
			</div>

		)
	}
}

