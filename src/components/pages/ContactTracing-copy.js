import '../../css/index.css'
import React from 'react';
import LivePlacesSearch from './Test'
import $ from 'jquery';
import DateTimePicker from 'react-datetime-picker';
import ReactDOM from 'react-dom';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import List from '../pages/tesMapfunc'
import Places from '../pages/locationBinding';

export default class ContactTracing extends React.Component {
    google = {};
    map = {};
    markers = [];
    localUser = '';
    locArr = [];
    constructor(props) {
        super(props);

        this.state = {
            data: '',
            locArr: [],
            phone: '',
            phoneArr: [],
            date: '',
            dateArray: [],
            userInfo: []

        }
        this.updateState = this.updateState.bind(this);
    };
    updateState(e) {
        this.setState({ data: e.target.value });
    }
    handleSubmit = (place) => {
        alert(place)

    }
    handleSubmitPhone = (event) => {
        this.state.phoneArr.push(this.state.phone);
        ReactDOM.findDOMNode(this.refs.phoneno).value = "";
    }
    handleSubmitMapAndDuration = () => {
        let location = { place: this.state.data, duration: this.state.date };

        this.locArr.push(location);
        this.addLocation(this.state.data);
        ReactDOM.findDOMNode(this.refs.loc).value = "";
        this.setState({
            date: ''
        })

    }
    handlePhoneChange = (event) => {

        this.setState({ phone: event.target.value });
    }
    handleDurationChange = (event) => {

        console.log(event)
        this.setState({ duration: event.target.value });
    }
    handleDurationChange2 = (event) => {

        this.setState({ date: event })

    }


    componentDidMount = () => {

        let user = JSON.parse(localStorage.getItem("user"));
        console.log(user);
        this.localUser = user.mobile;
        //fetching user details
        this.fetchData();
        if (!window.google) {
            this.loadMap().then((loaded) => {
                if (loaded) {
                    this.google = window.google;
                    // console.log(this.google)
                    this.initMap();
                }
            });
        }
        else {
            this.google = window.google;
            this.initMap();
        }
    }

    initMap() {
        let that = this;
        this.map = new this.google.maps.Map(document.getElementById('map'), {
            zoom: 10,
            center: new this.google.maps.LatLng(12.963588, 77.712155),
            mapTypeId: 'roadmap'
        });
        var input = document.getElementById('pac-input');

        var searchBox = new this.google.maps.places.SearchBox(input);

        searchBox.addListener('places_changed', function () {

            var places = searchBox.getPlaces();
            that.place = places[0];
            // console.log(places[0])

            that.setState({
                data: places[0]
            })
            //console.log(that.state.data)
            //that.addLocation(places[0])
        });
    }

    addLocation(places) {

        this.markers.push(new this.google.maps.Marker({ position: places.geometry.location, map: this.map }));

    }

    loadMap() {
        return new Promise((resolve) => {
            window.initMap = (ev) => {
                resolve(true);
            };
            var script = document.createElement('script');
            script.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyA35r-KTMbuDSLnJBJkyMh0moxmAPgqEAs&libraries=places&callback=initMap';
            script.type = 'text/javascript';
            script.async = true;
            script.defer = true;
            document.getElementsByTagName('head')[0].appendChild(script);
        });


    }


    submittingData = () => {
        // alert('gggg')
        console.log('djijd');
        console.log(this.state.locArr);
        console.log(this.state.phoneArr);
        console.log(this.state.dateArray);
        if (this.state.userInfo.mobile == '') {
            toast('Please Privide User Details');
        }
        else {
            this.submitTracing();
        }

    }

    fetchData = () => {
        fetch('http://139.59.17.158:8000/tracker/fetchUserData', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-type': 'application/json'
            },
            body: JSON.stringify(
                {
                    "mobile": this.localUser
                }
            ),
        })
            .then((response) => response.json())
            .then((responseJson) => {
                // console.log(responseJson);
                this.setState({
                    userInfo: responseJson
                });
                if (responseJson.hasOwnProperty('traces')) {
                    this.setState({
                        phoneArr: responseJson.traces.contacts
                    });
                }

                if (responseJson.mobile == '') {
                    toast('Please Privide User Details');
                }
            })
            .catch((error) => {
                console.error(error);
            });
    }


    submitTracing = () => {
        let reqdata = {
            mobile: this.localUser,
            traces: {}
        };
        if (this.state.phoneArr.length > 0) {
            reqdata.traces["contacts"] = this.state.phoneArr;
        }
        if (this.locArr.length > 0) {
            reqdata.traces["locations"] = this.locArr;
        }

        fetch('http://139.59.17.158:8000/tracker/contactTrace', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-type': 'application/json'
            },
            body: JSON.stringify(reqdata),
        })
            .then((response) => response.json())
            .then((responseJson) => {
                console.log(responseJson);
                toast("Successfully submitted");
            })
            .catch((error) => {
                console.error(error);
            });
    }

    render() {
        toast.configure({
            autoClose: 8000,
            draggable: false,
            //etc you get the idea
        });
        return (
            <div>
                <div className="header" style={{}}>
                    <div className="container">
                        <h5>Contact Tracing</h5>
                    </div>
                </div>
                <div className="container">

                    <div className="row">

                        <div className="col-sm-10 col-10 col-md-10">

                            <input type="text" className="form-control" ref="phoneno" placeholder="Mobile No." onChange={this.handlePhoneChange} style={{ marginBottom: 10 }} />
                        </div>
                        <div className="col-sm-2 col-2 col-md-2">
                            <button type="button" className="btn btn-plus" onClick={() => this.handleSubmitPhone()}>
                                <i className="fas fa-plus"></i>
                            </button>
                        </div>

                    </div>
                    <List phoneNo={this.state.phoneArr} />


                    <div className="row">
                        <div className="col-sm-4 col-12 col-md-3">
                            <div className="location">
                                <input id="pac-input" ref="loc" type="text" className="live-search-box form-control" placeholder="Search Location" />
                            </div>
                        </div>
                        <div className="col-sm-4 col-10 col-md-4 no-padding">
                            <DateTimePicker
                                onChange={this.handleDurationChange2}
                                value={this.state.date}
                            />

                        </div>
                        <Places places={this.state.locArr} />
                        <div className="col-sm-2 col-2 col-md-1">
                            <button type="button" className="btn btn-plus" onClick={() => this.handleSubmitMapAndDuration()}>
                                <i className="fas fa-plus"></i>
                            </button>
                        </div>
                        <div className="col-sm-4 col-12 col-md-4">
                            <div className="card">
                                <div className="map-block">
                                    <div id="map" className="map1" style={{}}>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>



                    <div className="container">
                        <div className="" style={{ textAlign: "center", marginBottom: 50, marginTop: 30 }}>
                            <button className="btn btn-success btn-submit" type="button" onClick={() => this.submittingData()}>SUBMIT</button>
                        </div>
                    </div>
                </div>
            </div>
        )


    }

}

