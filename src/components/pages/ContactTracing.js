import '../../css/index.css'
import React from 'react';
import LivePlacesSearch from './Test'
import $ from 'jquery';
import DateTimePicker from 'react-datetime-picker';
import ReactDOM from 'react-dom';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import List from '../pages/tesMapfunc'
import Places from '../pages/locationBinding';

export default class ContactTracing extends React.Component {
    google = {};
    map = {};
    markers = [];
    localUser = '';
    locArr = [];
    constructor(props) {
        super(props);

        this.state = {
            data: '',
            locArr: [],
            phone: '',
            phoneArr: [],
            date: '',
            dateArray: [],
            userInfo: [],
            numberAdded: false,
            placename: [],
            placeAdded: false,
            lat: '12.963588',
            lng: '77.712155'

        }
        this.updateState = this.updateState.bind(this);
    };
    updateState(e) {
        this.setState({ data: e.target.value });
    }
    handleSubmit = (place) => {
        alert(place)

    }
    handleSubmitPhone = (event) => {
        let num =this.state.phone;
        if (num.match(/^\d{10}$/)) {
            
            this.state.phoneArr.push(this.state.phone);
            ReactDOM.findDOMNode(this.refs.phoneno).value = "";
            this.setState({
                numberAdded: true
            })
        }
        else{
            
             document.getElementById("Phoneerror").innerHTML = "Please enter a 10-digit Phone Number";
            
        }
      
       
    }
    handleSubmitMapAndDuration = () => {
        let location = { place: this.state.data, duration: String(this.state.date), placeName: this.state.data.name };

        this.locArr.push(location);
        //console.log(this.locArr[0].place.name);
        //    this.locArr.map((place, index) => {
        //console.log(place.duration);
        //console.log(place.placeName);
        //       })

        this.addLocation(this.state.data);
        ReactDOM.findDOMNode(this.refs.loc).value = "";
        this.setState({
            date: '',
            placeAdded: true
        })


    }
    handlePhoneChange = (event) => {
        document.getElementById("Phoneerror").innerHTML = "";

        this.setState({ phone: event.target.value });
    }
    handleDurationChange = (event) => {

        // console.log(event)

        
        this.setState({ duration: event.target.value });
    }
    handleDurationChange2 = (event) => {

        this.setState({ date: event })

    }


    componentDidMount = () => {
        $("#back").show();
        let user = JSON.parse(localStorage.getItem("user"));
        //console.log(user);
        this.localUser = user.mobile;
        //fetching user details
        this.fetchData();

        if (window.google) {

            this.google = window.google;
            // console.log(this.google)
            this.initMap();

        }

    }

    initMap() {
        let that = this;
        this.map = new this.google.maps.Map(document.getElementById('map'), {
            zoom: 8,
            center: new this.google.maps.LatLng(this.state.lat, this.state.lng),
            mapTypeId: 'roadmap'
        });
        var input = document.getElementById('pac-input');
        var searchBox = new this.google.maps.places.SearchBox(input);

        searchBox.addListener('places_changed', function () {
            var places = searchBox.getPlaces();
            that.place = places[0];

            that.setState({
                data: places[0],
                lat:places[0].geometry.location.lat(),
                lng:places[0].geometry.location.lng()
           })
    
        });
    }

    addLocation(places) {
        this.map.setCenter(places.geometry.location);
        this.markers.push(new this.google.maps.Marker({ position: places.geometry.location, map: this.map }));

    }

    loadMap() {
        return new Promise((resolve) => {
            window.initMap = (ev) => {
                resolve(true);
            };
            var script = document.createElement('script');
            script.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyB7s8nphRNHn90429k_pVywvt-QOhyWSJk&libraries=places&callback=initMap';
            script.type = 'text/javascript';
            script.async = true;
            script.defer = true;
            document.getElementsByTagName('head')[0].appendChild(script);
        });


    }


    submittingData = () => { 
            this.submitTracing();
  
    }

    fetchData = () => {
        fetch('http://139.59.17.158:8000/tracker/fetchUserData', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-type': 'application/json'
            },
            body: JSON.stringify(
                {
                    "mobile": this.localUser
                }
            ),
        })
            .then((response) => response.json())
            .then((responseJson) => {
                // console.log(responseJson);
                this.setState({
                    userInfo: responseJson
                });
                if (responseJson.hasOwnProperty('traces')) {
                    this.setState({
                        phoneArr: responseJson.traces.contacts
                    });
                }

                if (responseJson.mobile == '') {
                    toast('Please Privide User Details');
                }
            })
            .catch((error) => {
                console.error(error);
            });
    }


    submitTracing = () => {
        let reqdata = {
            mobile: this.localUser,
            traces: {}
        };
        if (this.state.phoneArr.length > 0) {
            reqdata.traces["contacts"] = this.state.phoneArr;
        }
        if (this.locArr.length > 0) {
            reqdata.traces["locations"] = this.locArr;
        }

        fetch('http://139.59.17.158:8000/tracker/contactTrace', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-type': 'application/json'
            },
            body: JSON.stringify(reqdata),
        })
            .then((response) => response.json())
            .then((responseJson) => {
                // console.log(responseJson);
                toast("Successfully submitted");
                this.props.history.push("/");
            })
            .catch((error) => {
                console.error(error);
            });
    }

    render() {
        toast.configure({
            autoClose: 8000,
            draggable: false,
            //etc you get the idea
        });
        return (
            <div>
                <div className="header" style={{}}>
                    <div className="container">
                        <h5>Contact Tracing</h5>
                    </div>
                </div>
                <div className="container">

                    <div className="row">

                        <div className="col-sm-10 col-10 col-md-10">

                            <input type="text" className="form-control" ref="phoneno" placeholder="Add Mobile nos. of people whom you have contacted" onChange={this.handlePhoneChange} style={{ marginBottom: 10 }} />
                            <span id="Phoneerror" style={{color:'red'}}></span>
                        </div>
                        <div className="col-sm-2 col-2 col-md-2">
                            <button type="button" className="btn btn-plus" onClick={() => this.handleSubmitPhone()}>
                                <i className="fas fa-plus"></i>
                            </button>
                        </div>

                    </div>

                    {(this.state.phoneArr.length > 0 && this.state.numberAdded) && (<div className="card stack-card">
                        <ul>
                            {this.state.phoneArr.map((person, index) => (

                                <li key={index} className="card crd-no">

                                    <p>{person}</p>
                                    <span className="icon"><i className="fas fa-times"></i></span>
                                </li>
                            ))}
                        </ul>
                    </div>
                    )}
                    <div className="row">
                        <div className="col-sm-12 col-12 col-md-12">
                            <div className="row">
                                <div className="col-sm-4 col-12 col-md-4">
                                    <div className="location">
                                        <input id="pac-input" ref="loc" type="text" className="live-search-box form-control" placeholder="Add places you have visited in last 14 days" />
                                    </div>
                                </div>
                                <div className="col-sm-6 col-10 col-md-6 no-padding">

                                    <DateTimePicker
                                        onChange={this.handleDurationChange2}
                                        value={this.state.date}
                                    />
                                  {/*<input type='text'   className="form-control" id='datetimepicker1' placeholder="Duration" />*/}
                                </div>

                                <div className="col-sm-2 col-2 col-md-1">
                                    <button type="button" className="btn btn-plus" onClick={() => this.handleSubmitMapAndDuration()}>
                                        <i className="fas fa-plus"></i>
                                    </button>
                                </div>


                            </div>

                        </div>
                    </div>
                    {(this.locArr.length > 0 && this.state.placeAdded) && (<div className="card stack-card">

                        <ul>
                            {this.locArr.map((place, index) => (

                                <li key={index} className="card crd-no">

                                    <span>{place.placeName} - {place.duration}</span>
                                    <span className="icon"><i className="fas fa-times"></i></span>
                                </li>
                            ))}
                        </ul>
                    </div>
                    )}
                    <div className="row">
                        <div className="col-sm-12 col-12 col-md-12">

                            <div className="map-block tracing-map">
                                <div id="map" className="map1" style={{}}>
                                </div>
                            </div>
                        </div>
                    </div>





                    <div className="container">
                        <div className="" style={{ textAlign: "center", marginBottom: 50, marginTop: 30 }}>
                            <button className="btn btn-success btn-submit" type="button" onClick={() => this.submittingData()}>SUBMIT</button>
                        </div>
                    </div>
                </div>
            </div>
        )


    }

}

