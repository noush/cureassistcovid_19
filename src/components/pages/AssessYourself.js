import React from 'react';
import $ from 'jquery';
import '../../css/index.css'
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import ReactDOM from 'react-dom';
import Suggestion from './suggestion';
//import Modal from 'react-responsive-modal';
//import Modal from '../modal/modal';
import Modal from '../modal/modal_new';

export default class Home extends React.Component {
    symptomChkd = [];
    contacted = [];
    localUser = {};
    travelledCountry = [];
    travelledFaq = [];
    contactFaq = [];
    symFaq = [];
    value = [];
    pos = {};
    lat = '';
    lng = '';
    constructor() {
        super();
        this.state = {
            toggleActivecontact: false,
            toggleActivetravel: false,
            age: '',
            sex: 'Male',
            contacted: 'no',
            travll: 'no',
            txtarea: '',
            option1suggtn: false,
            feverOrCough: false,
            shortnssPluss: false,
          
        }
        this.onToggleContact = this.onToggleContact.bind(this);
        this.onToggleTravell = this.onToggleTravell.bind(this);
    }


    componentDidMount = () => {

        $("#back").show();
        //getting local user details
        let user = JSON.parse(localStorage.getItem("user"));
        this.localUser = user;
        //console.log(this.localUser)

        if (!window.google) {
            this.loadMap().then((loaded) => {
                if (loaded) {
                    this.google = window.google;
                    // console.log(this.google)
                    //  this.initMap();
                }
            });
        }
        else {
            this.google = window.google;
            //this.initMap();
        }


        //checking whwther user has already 
        this.fetchData();

        let that = this;
        $('.nav-list li').click(function () {

            $(this).toggleClass('active');

            if (that.symptomChkd.includes($(this).data('interest'))) {
                const index = that.symptomChkd.indexOf($(this).data('interest'));
                if (index > -1) {
                    that.symptomChkd.splice(index, 1);
                }
            }
            else {
                that.symptomChkd.push($(this).data('interest'));
            }
            //  console.log(that.symptomChkd);
        });

        $('.nav-list li').click(function () {
            $(this).toggleClass('active');

            if (that.contacted.includes($(this).data('interest'))) {
                const index = that.contacted.indexOf($(this).data('interest'));
                if (index > -1) {
                    that.contacted.splice(index, 1);
                }
            }
            else {
                that.contacted.push($(this).data('interest'));
            }
            // console.log(that.contacted);
        });

        $('.nav-list li').click(function () {
            $(this).toggleClass('active');

            if (that.travelledCountry.includes($(this).data('interest'))) {
                const index = that.travelledCountry.indexOf($(this).data('interest'));
                if (index > -1) {
                    that.travelledCountry.splice(index, 1);
                }
            }
            else {
                that.travelledCountry.push($(this).data('interest'));
            }
            // console.log(that.travelledCountry);
        });
        $('.nav-list li').click(function () {
            $(this).toggleClass('active');

            if (that.travelledCountry.includes($(this).data('interest'))) {
                const index = that.travelledCountry.indexOf($(this).data('interest'));
                if (index > -1) {
                    that.travelledCountry.splice(index, 1);
                }
            }
            else {
                that.travelledCountry.push($(this).data('interest'));
            }
            // console.log(that.travelledCountry);
        });

    }


    loadMap() {
        return new Promise((resolve) => {
            window.initMap = (ev) => {
                resolve(true);
            };
            var script = document.createElement('script');
            script.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyB7s8nphRNHn90429k_pVywvt-QOhyWSJk&libraries=places&callback=initMap';
            script.type = 'text/javascript';
            script.async = true;
            script.defer = true;
            document.getElementsByTagName('head')[0].appendChild(script);
        });


    }
    initMap() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition((position) =>{
                    this.lat = position.coords.latitude;
                    this.lng = position.coords.longitude;
                
            })
        }
    }


    fetchData = () => {
        fetch('http://139.59.17.158:8000/tracker/fetchUserData', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-type': 'application/json'
            },
            body: JSON.stringify(
                {
                    "mobile": this.localUser.mobile
                }
            ),
        })
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({
                    userInfo: responseJson
                })
                if (responseJson.mobile == '') {
                    toast('Please Privide User Details');
                }
            })
            .catch((error) => {
                console.error(error);
            });
    }


    onToggleContact(event) {
        this.setState({ toggleActivecontact: !this.state.toggleActivecontact });
    }

    onToggleTravell(event) {
        this.setState({ toggleActivetravel: !this.state.toggleActivetravel });
    }

    // optionChange = (val) => {
    //     this.setState({ option1suggtn: val});
    //  }

    onSubmit = () => {

        // var symptm = this.symptomChkd.filter(data => data.includes('Fever') || data.includes('Cough')
        //     || data.includes('Fatigue') || data.includes('Shortness of Breath'));
        // if (symptm != '') {
        //     this.symFaq.push(symptm);
        // }
        //   console.log(this.symFaq);

         this.SaveAssess();

    }
    onCloseModal = () => {
        this.setState({ option1suggtn: false });
    };
    test = (val) => {
        this.setState({
            feverOrCough: val
        })

    }
    option2 = (val) => {
        this.setState({
            shortnssPluss: val
        })
    }
    SaveAssess = () => {
        let symptoms = this.symFaq;
        let contcted = this.contactFaq;
        let travell = this.travelledFaq;
        fetch('http://139.59.17.158:8000/tracker/selfassess', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-type': 'application/json'
            },
            body: JSON.stringify(
                {
                    "mobile": this.localUser.mobile,
                    "questions": [
                        {
                            "question": "Enter your age?",
                            "answer": this.state.age
                        },
                        {
                            "question": "Enter your Gender?",
                            "answer": this.state.travll
                        },
                        {
                            "question": "Select all symptoms that you have?",
                            "answer": this.value
                        },
                        {
                            "question": "Have you come in close contact with a person known to have  COVID-19 illness  in last  14 days.?",
                            "answer": this.state.contacted
                        },
                        {
                            "question": "Have you recently  in last  1 month  travelled  to Corona Virus (Covid-19) affected countries?",
                            "answer": this.state.travll
                        },
                        {
                            "question": "History of Health Condition ?",
                            "answer": this.state.txtarea
                        }
                    ]
                }
            ),
        })
            .then((response) => response.json())
            .then((responseJson) => {
                // console.log(responseJson);
                let feverOrCough = this.value.includes("Fever") || this.value.includes("Cough");
                let feverNCough = this.value.includes('Fever') && this.value.includes('Cough');
                let shortnessNFever = this.value.includes('Shortness of Breath') && this.value.includes('Fever');
                let shortness = this.value.includes('Shortness of Breath');
                toast(" Successfully Saved Assesment");
                if (this.state.travll == 'yes' || this.state.contacted == 'yes' ||feverNCough || shortness) {
                    this.option2(true);
                }
                else if(feverOrCough){
                    this.test(feverOrCough);
                }

                // if (option1) {
                //     if (feverNCough) {
                //         this.option2(true);
                //     }
                //     else {
                //         if (shortnessNFever) {
                //             this.option2(true);
                //         }
                //         else {
                //             if (this.state.travll == 'yes' || this.state.contacted == 'yes') {
                //                 this.option2(true);
                //             }else{
                //                 this.test(option1);
                //             }
                            
                //         }
                //     }
                // }
                // else {
                //     if (feverNCough || shortnessNFever || shortness) {

                //         this.option2(true);
                //     }
                // }
                //for travell and cloce contact
               
                if (this.state.travll == 'yes' || this.state.contacted == 'yes') {
                    // console.log('yes clicked')
                    
                    this.initMap();
                    setTimeout(()=>{if(this.lat != '' && this.lng != ''){
                        this.setUserData();
                    }
                },6000);
                  
                }
           
            })
            .catch((error) => {
                console.error(error);
            });
    }

    setUserData = () =>{
        fetch('http://139.59.17.158:8000/tracker/saveProfile', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-type': 'application/json'
            },
            body: JSON.stringify({
                "mobile": this.localUser.mobile,
                "user": {
                    "name": this.localUser.user.name,
                    "email": this.localUser.user.email,
                    "lat": this.lat,
                    "lng" : this.lng
                }
            }),
        })
            .then((response) => response.json())
            .then((responseJson) => {

                toast("User Profile Saved");

            })
            .catch((error) => {
                toast("Error Saving User Profile ");
            });
    }

    handleAge = (event) => {
        this.setState({ age: event.target.value });
    }

    contact = (event) => {

        this.setState({ contacted: event.target.value });
    }
    symptoms = (e) => {
        var options = e.target.options;
        this.value = [];
        for (var i = 0, l = options.length; i < l; i++) {
            if (options[i].selected) {

                this.value.push(options[i].value);
            }
        }
        // console.log(this.value)
    }
    travelled = (event) => {

        this.setState({ travll: event.target.value });
    }
    gender = (event) => {

        this.setState({ sex: event.target.value });
    }
    additionalInfo = (event) => {

        this.setState({
            txtarea: event.target.value
        })
    }
    render() {
        toast.configure({
            autoClose: 8000,
            draggable: false,
            //etc you get the idea
        });

        return (
            <div>
                <div className="assess-block">
                    <div className="header" style={{}}>
                        <div className="container">
                            <h5>Self Assessment</h5>
                        </div>
                    </div>

                    <div className="container">
                        <div className="row">
                            <div className="col-12 col-md-6 col-sm-6">
                                <div className="card" style={{ padding: 10 }}>
                                    <label>Enter your age</label>
                                    <input type="text" className="form-control" placeholder="Enter your age" onChange={this.handleAge} />
                                </div>
                            </div>
                            <div className="col-12 col-md-6 col-sm-6">
                                <div className="card " style={{ padding: 10 }}>
                                    <label>Enter your gender</label>

                                    <select className="form-control" onChange={this.gender}>
                                        <option value="Male">Male</option>
                                        <option value="Female">Female</option>
                                        <option value="others">Others</option>
                                    </select>


                                </div>
                            </div>
                        </div>
                        <div className="card" style={{ padding: 10 }}>
                            <label>Select all symptoms that you have</label>
                            <select className="form-control" multiple onChange={this.symptoms}>
                                <option value="Fever">Fever</option>
                                <option value="Cough">Cough</option>
                                <option value="Fatigue">Fatigue</option>
                                <option value="Shortness of Breath">Shortness of Breath</option>
                                <option value="no symptoms">None</option>
                            </select>
                        </div>

                        <div className="card" style={{ padding: 10, marginBottom: 20 }}>
                            <label>Have you come in close contact with a person known to have  COVID-19 illness  in last  14 days.</label>

                            <select className="form-control" onChange={this.contact}>
                            <option value="no">NO</option>
                                <option value="yes">YES</option>
                            </select>




                        </div>

                        <div className="card" style={{ padding: 10 }}>
                            <label>Have you recently  in last  1 month  travelled  to Corona Virus (Covid-19) affected countries.</label>
                            <select className="form-control" onChange={this.travelled}>
                                <option value="no">NO</option>
                                <option value="yes">YES</option>
                            </select>


                        </div>
                        <div className="card" style={{ padding: 10 }}>
                            <label>History of Health Condition </label>
                            <textarea className="form-control" id="exampleFormControlTextarea1" rows="3" onChange={this.additionalInfo}></textarea>
                        </div>




                    </div>

                    <div className="container">
                        <div className="" style={{ textAlign: "center", marginBottom: 50 }}>
                        <button id="submit-id" className="btn btn-success btn-submit" type="button" onClick={() => this.onSubmit()}>SUBMIT</button>
                            {/* <button id="submit-id" className="btn btn-success btn-submit" data-toggle="modal" data-target="#exampleModal" type="button" onClick={() => this.onSubmit()}>SUBMIT</button> */}
                        </div>
                    </div>

                    <div>
                        {this.state.feverOrCough && (<Modal closeModal={() => {
                            this.setState({ feverOrCough: false });
                            this.props.history.push('/');
                    }}>
                            <h5>Your answers suggest that you are unlikely to be infected with COVID-19, but based on your symptoms you may have an upper respiratory tract virus.</h5>
                            <hr></hr>
                            <h6>According to official instructions, you should act as follows:</h6>
                            <div>
                                <ul className="list">
                                    <li>
                                        Stay at home for two weeks after symptoms emerge.
                                    </li>
                                    <li>
                                        Monitor your health! If the condition worsens (breathing difficulties; fever > 100 degrees with dry cough) visit your doctor
                                    </li>
                                    <li>
                                        If you are over 60 years old or you suffer from a chronic illness such as diabetes, lung disease or heart disease, monitor your health closely. If your symptoms persist beyond three days or worsen (fever >100degrees, breathing difficulties or general deterioration), contact a general practitioner immediately.
</li>
                                </ul>
                            </div>
                        </Modal>)
                        }
                    </div>

                    <div>
                        {this.state.shortnssPluss && (<Modal closeModal={() => {
                            this.setState({ shortnssPluss: false });
                            this.props.history.push('/');}}>
                            <h5>You have not knowingly been exposed to COVID-19, but your symptoms are indicative of a respiratory virus, which may be COVID-19.</h5>
                            <hr></hr>
                            <h6>According to official instructions, you should act as follows:</h6>
                            <div>
                                <ul className="list">
                                    <li>
                                        Remain in-home care for two weeks.
                                    </li>
                                    <li>
                                        Contact your general practitioner.
                                    </li>
                                    <li>
                                        Monitor your health!
                                    </li>
                                    <li>
                                        Inform those you have been in close contact with to monitor their health, stay home if possible, and to fill out this self-assessment questionnaire for further instructions if necessary
                                    </li>
                                </ul>
                            </div>
                        </Modal>)
                        }
                    </div>

                </div>

            </div>
        )
    }

}