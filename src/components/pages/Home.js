import React from 'react';
import { Link, Redirect } from 'react-router-dom';
import img from '../../images/icon-1.png';
import img2 from '../../images/icon-3.png';
import img3 from '../../images/icon-5.png';
import img4 from '../../images/icon-6.png';
import img5 from '../../images/icon-7.png';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import $ from 'jquery'


export default class Home extends React.Component {

    componentDidMount = () => {
        this.test1();
        $("#back").hide();
        $("#userDetails").show();
    }

    hideDiv = (id) => {
        $("#" + id).hide();
    }
    test1 = () => {
        let disclaimerCheck = localStorage.getItem("alreadyVisited");
     //   console.log(disclaimerCheck)
        if (disclaimerCheck === 'yes') {

            $("#disclaimer").hide();
        }
        else {
            localStorage.setItem("alreadyVisited", "yes");
            $("#disclaimer").show();
        }

    }

    NavigateToRoute = (route) => {
        let user = JSON.parse(localStorage.getItem("user"));
        if (user == null) {
            toast("Please provide User Details");
            toast.configure({
                autoClose: 8000,
                draggable: false,
                //etc you get the idea
            });
        }
        else {
            this.props.history.push(route);
        }
    }
    render() {

        return (

            <div>
                <section className="card-list">
                    <div className="container">
                        <div className="row">
                            <div className="col-12 col-sm-6 col-md-6 ">
                                <div onClick={() => this.NavigateToRoute("/assess")}>
                                    <div className="card btn btn-outline-light card-list1">
                                        <div className="row align-items-center">
                                            <div className="col-sm-4 col-4 col-md-4">
                                                <img width="100" src={img} alt="" />
                                            </div>
                                            <div className="col-sm-8 col-8 col-md-8">
                                                <div className="card-text">
                                                    <span>Self Assessment <i className="fas fa-chevron-right"> </i> </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-12 col-sm-6 col-md-6">
                                <div onClick={() => this.NavigateToRoute("/tracker")}>
                                    <div className="card btn btn-outline-light card-list1">
                                        <div className="row align-items-center">
                                            <div className="col-sm-4 col-4 col-md-4">
                                                <img width="100" src={img2} alt="" />
                                            </div>
                                            <div className="col-sm-8 col-8 col-md-8">
                                                <div className="card-text" style={{ wordWrap: "break-word" }}>
                                                    <span style={{ flexWrap: 'wrap' }}>Contact Tracing <i className="fas fa-chevron-right"> </i></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-12 col-sm-6 col-md-6">
                            <div onClick={() => this.props.history.push("/map")}>
                                    <div className="card btn btn-outline-light card-list1">
                                        <div className="row align-items-center">
                                            <div className="col-sm-4 col-4 col-md-4">
                                                <img width="100" src={img3} alt="" />
                                            </div>
                                            <div className="col-sm-8 col-8 col-md-8">
                                                <div className="card-text">
                                                    <span>COVID-19 Heatmap<i className="fas fa-chevron-right"> </i></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-12 col-sm-6 col-md-6">
                            <div onClick={() => this.props.history.push("/emergency")}>
                                    <div className="card btn btn-outline-light card-list1">
                                        <div className="row align-items-center">
                                            <div className="col-sm-4 col-4 col-md-4">
                                                <img width="100" src={img4} alt="" />
                                            </div>
                                            <div className="col-sm-8 col-8 col-md-8">
                                                <div className="card-text">
                                                    <span>Emergency Contact <i className="fas fa-chevron-right"> </i></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                       

                        </div>
                    </div>
                </section>
                <div id="disclaimer" className="cookies animated slideInUp ">
                                <div className="container">
                                    <h4 style={{}}>Disclaimer : </h4>
                                    <h5>Coronavirus questionnaire</h5>
                                    <p>This short questionnaire will help assess your risk of being infected and provides recommendations based on your situation.</p>
                                    <p>Your answers, in turn, help us assess and predict the spread of the virus in India. By answering the survey, you will receive further suggestions on how to behave in your situation. By submitting this questionnaire, I agree to my IP address being logged for the Indian Health Board to differentiate between multiple entries. Your IP will not be used to identify you.</p>
                                    <h5>PRIVACY</h5>
                                    <p>The phone number collected, will not be used to identify or discriminate against you. The Indian Health Board will use your location data until the end of the COVID-19 epidemic in India to detect local outbreaks and direct resources. Your location data will not be used to identify you. Following the end of the epidemic, your location data shall be converted to the level of your local government and retained for a period of no more than three years in order to assess the effectiveness of local containment efforts and to draw relevant conclusions. </p>

                                    <button id="close-id" type="button" className="btn btn-default btn-close" style={{ float: 'right' }} onClick={() => this.hideDiv('disclaimer')}>CLOSE</button>
                                </div>
                            </div>
            </div>
        );
    }
}

