import React from 'react';
import '../../css/index.css'
class App extends React.Component {
   constructor(props) {
      super(props);

      this.state = {
         data: 'Initial data...'
      }
      this.updateState = this.updateState.bind(this);
   };
   updateState(e) {
      this.setState({ data: e.target.value });
   }
   render() {
      return (
         <div>
            <Content myDataProp={this.state.data}
               updateStateProp={this.updateState}></Content>
            <div id="map" style={{ marginLeft: 50, marginRight: 50, height: 100 }}></div>
         </div>

      );
   }
}
class Content extends React.Component {
   google = {};
   map = {};
   markers = [];
   handleSubmit = (place) => {
      alert(place)
      //    let map = new google.maps.Map(document.getElementById('map'), {
      //         center: {lat: -34.397, lng: 150.644},//based in what
      //         zoom: 8
      //       });
   }
   componentDidMount() {
      this.loadMap().then((loaded) => {
         if (loaded) {
            this.google = window.google;
            // console.log(this.google)
            this.initMap();
         }
      });
   }

   initMap() {
      let that = this;
      this.map = new this.google.maps.Map(document.getElementById('map'), {
         zoom: 10,
         center: new this.google.maps.LatLng(12.963588, 77.712155),
         mapTypeId: 'roadmap'
      });
      var input = document.getElementById('pac-input');

      var searchBox = new this.google.maps.places.SearchBox(input);

      searchBox.addListener('places_changed', function () {

         var places = searchBox.getPlaces();
         that.place = places[0];
         console.log(places[0])
         that.addLocation(places[0])
      });
   }

   addLocation(places) {

      this.markers.push(new this.google.maps.Marker({ position: places.geometry.location, map: this.map }));

   }

   loadMap() {
      return new Promise((resolve) => {
         window.initMap = (ev) => {
            resolve(true);
         };
         var script = document.createElement('script');
         script.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyB7s8nphRNHn90429k_pVywvt-QOhyWSJk&libraries=places&callback=initMap';
         script.type = 'text/javascript';
         script.async = true;
         script.defer = true;
         document.getElementsByTagName('head')[0].appendChild(script);
      });


   }

   render() {
      return (
         
         <div className="container">
         <form onSubmit={() => this.handleSubmit(this.props.myDataProp)}>
            <input id="pac-input" placeholder="Search Location" type="text" style={{ marginTop: 10, width: 400, marginLeft: 50 }} />
         </form>
         </div>
      );
   }
}



export default App;