import React from 'react'
import animate from "animate.css"
import $ from 'jquery'


export default class Emergency extends React.Component {

	componentDidMount = () => {
		$("#back").show();
	}


	/*render() {
		return (
			<div>
				<div className="header" style={{}}>
					<div className="container">
						<h5>Emergency Contact </h5>
					</div>
				</div>
				<section className=" faq-list">
					<div className="container">

						<div className="card">
							<label>The Helpline Number Toll free Number <i className="fa fa-phone" style={{ marginLeft: 10 }}></i></label>
							<p><a href="tel:1075" target="_blank">1075</a></p>
							<p><a href="tel:+91-11-23978046" target="_blank">+91-11-23978046</a></p>
							<hr />
							<label>The Helpline Email ID for corona-virus <i className="fas fa-envelope"></i></label>
							<p><a href="mailto:ncov2019@gov.in" target="_blank" title="">ncov2019@gov.in</a></p>
							<p><a href="mailto:ncov2019@gmail.com" target="_blank" title="">ncov2019@gmail.com</a></p>
		</div>*/

	render() {
		return (
			<div>
				<div className="header" style={{}}>
					<div className="container">
						<h5>Emergency Contact </h5>
					</div>
				</div>
				<section className=" faq-list">
					<div className="container">

						<div className="card">
							<label>The Helpline Number Toll free Number<i className="fa fa-phone" style={{ marginLeft: 10 }}></i></label>
							<p><a href="tel:1075" target="_blank">1075</a></p>
							<p><a href="tel:+91-11-23978046" target="_blank">+91-11-23978046</a></p>

							<hr />
							<label>The Helpline Email ID for corona-virus <i className="fas fa-envelope"></i></label>
							<p><a href="mailto:ncov2019@gov.in">ncov2019@gov.in</a></p>
							<p><a href="mailto:ncov2019@gmail.com">ncov2019@gmail.com</a></p>


							{/* <label>For more information please <a href="https://www.sst.dk/da/corona-eng/Hotlines" target="_blank"> click here</a></label> */}

						</div>

					</div>
				</section>


			</div>
		)
	}
}