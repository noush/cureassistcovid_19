import React from 'react';
import logo from './logo.svg';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle.min.js';
import 'bootstrap/dist/js/bootstrap.min.js';
import './css/index.css';
import { Switch, Route } from 'react-router-dom';
import Header from './components/layouts/Navbar';
import Footer from './components/layouts/Footer';
import Home from './components/pages/Home';
import HeatMap from './components/pages/HeatMap';
import Tracker from './components/pages/Tracker';
import Test from './components/pages/Test'
import AssessYourself from './components/pages/AssessYourself'
import ContactTracking from './components/pages/ContactTracing'
import Emergency from './components/pages/Emergency'
import List from './components/pages/tesMapfunc'
function App() {
    return (
        <div>
            <Header />
            <Switch>
                <Route exact path="/" component={Home} /> 
                <Route  path="/dashboard" component={Home} /> 
                <Route path="/map" component={HeatMap} />
                <Route path="/tracker" component={ContactTracking} />
                <Route path="/test" component={Test} />
                <Route path="/assess" component={AssessYourself} />
                 <Route path="/test2" component={Tracker} />
                 <Route path="/emergency" component={Emergency} />
                 <Route path="/list" component={List} />
            </Switch>
            <Footer />
        </div>


    );
}

export default App;

