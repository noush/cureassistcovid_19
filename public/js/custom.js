$(document).ready(function () {
  $(".lang-flag").click(function () {
    $(".language-dropdown").toggleClass("open");
  });
  $("ul.lang-list li").click(function () {
    $("ul.lang-list li").removeClass("selected");
    $(this).addClass("selected");
    if ($(this).hasClass('lang-en')) {
      $(".language-dropdown").find(".lang-flag").addClass("lang-en").removeClass("lang-es").removeClass("lang-pt");
      $("#lang_selected").html("<p>EN</p>")
    } else if ($(this).hasClass('lang-pt')) {
      $(".language-dropdown").find(".lang-flag").addClass("lang-pt").removeClass("lang-es").removeClass("lang-en");
      $("#lang_selected").html("<p>PT</p>")
    } else {
      $(".language-dropdown").find(".lang-flag").addClass("lang-es").removeClass("lang-en").removeClass("lang-pt");
      $("#lang_selected").html("<p>ES</p>")
    }
    $(".language-dropdown").removeClass("open");
  });


  /*counter*/
  $('.counter').each(function () {
    var $this = $(this),
      countTo = $this.attr('data-count');

    $({ countNum: $this.text() }).animate({
      countNum: countTo
    },

      {

        duration: 8000,
        easing: 'linear',
        step: function () {
          $this.text(Math.floor(this.countNum));
        },
        complete: function () {
          $this.text(this.countNum);
          //alert('finished');
        }

      });
  });


});

/*live search*/
jQuery(document).ready(function ($) {

  $('.live-search-list li').each(function () {
    $(this).attr('data-search-term', $(this).text().toLowerCase());
  });

  $('.live-search-box').on('keyup', function () {

    var searchTerm = $(this).val().toLowerCase();

    $('.live-search-list li').each(function () {

      if ($(this).filter('[data-search-term *= ' + searchTerm + ']').length > 0 || searchTerm.length < 1) {
        $(this).show();
      } else {
        $(this).hide();
      }

    });

  });

});
$(function () {
  
});

$(document).ready(function () {

  // $('body').noisy({
  //     intensity: 0.2,
  //     size: 200,
  //     opacity: 0.28,
  //     randomColors: false, // true by default
  //     color: '#000000'
  // });

  $('.nav-list li').click(function () {
    $(this).toggleClass('active');
  });

  /*$('#submit-id').on('click', function () {

    $('body').addClass('bodyOverflow');
  });

  $("#times-id").click(function(){
    $("body").removeClass("bodyOverflow");
  });*/
  
});

$(function () {
  $('#datetimepicker1').datetimepicker();
 
});

